#pragma strict
#include "Output.h"


Output::Output(void)
	{
	}


Output::~Output(void)
	{
	}

void Output::print(float lamda, float mu, float totalTime, Server server)
	{
	float allCostumersCalculatedArrivalTime = 0.0f;
	float allCostumersCalculatedServiceTime = 0.0f;
	float allCostumersTimeSpentWaiting = 0.0f;
	for (int i=0; i<server.finishedCostumers.size(); i++)
		{
		Costumer thisCostumer = server.finishedCostumers[i];
		allCostumersCalculatedArrivalTime += thisCostumer.calculatedRandomArrivalTime;
		allCostumersCalculatedServiceTime += thisCostumer.calculatedRandomServiceTime;
		allCostumersTimeSpentWaiting += thisCostumer.timeSpentWaiting;
		float arrival = thisCostumer.arrivalTime;
		float wait = thisCostumer.timeSpentWaiting - thisCostumer.calculatedRandomServiceTime;
		float randomArrival = thisCostumer.calculatedRandomArrivalTime;
		float randomService = thisCostumer.calculatedRandomServiceTime;
		printf("Costumer #%d \t================================================================\n", i);
		printf("Arrived at %f, \tWaited for %f before being served.\n", arrival, wait);
		printf("Randomized Arrival Time:%f \tRandomized Servive Time: %f\n\n", randomArrival, randomService);
		}

	float meanCalculatedArrival = allCostumersCalculatedArrivalTime/server.finishedCostumers.size();
	float meanCalculatedService = allCostumersCalculatedServiceTime/server.finishedCostumers.size();
	float meanTimeSpentWaiting = allCostumersTimeSpentWaiting/server.finishedCostumers.size();
	float meanQueueLengthsWithoutActualCostumer = server.summedQueueLengthsWithoutActualCostumer / totalTime;
	float meanQueueLengths = server.summedQueueLengths / totalTime;


	printf("\n\n");
	printf(" =====================================================================\n");
	printf(" ============================= M/M/1 QUEUE ===========================\n");
	printf(" =====================================================================\n");
	printf(" ===                                                               ===\n");
	printf(" === Number of costumers: %d                            \t   ===\n", server.finishedCostumers.size());
	printf(" === Arrival rate lamda: %f                            \t   ===\n", lamda);
	printf(" === Service times mu: 1/%f                            \t   ===\n", mu);
	printf(" === Total needed Time: %f                            \t   ===\n", totalTime);
	printf(" === Average calculated arrival: %f                   \t   ===\n", meanCalculatedArrival);
	printf(" === Average calculated service: %f                   \t   ===\n", meanCalculatedService);
	printf(" === Average time spent waiting: %f                   \t   ===\n", meanTimeSpentWaiting);
	printf(" === Average queue length: %f                         \t   ===\n", meanQueueLengths);
	printf(" === Average queue length without the served one: %f  \t   ===\n", meanQueueLengthsWithoutActualCostumer);
	printf(" ===                                                               ===\n");
	printf(" =====================================================================\n");
	printf(" =====================================================================\n");
	}

void Output::outputToFile(float lamda, float mu, float totalTime, Server server)
	{
	std::ofstream myFile;
	myFile.open("Output.txt" );

	float allCostumersCalculatedArrivalTime = 0.0f;
	float allCostumersCalculatedServiceTime = 0.0f;
	float allCostumersTimeSpentWaiting = 0.0f;
	for (int i=0; i<server.finishedCostumers.size(); i++)
		{
		Costumer thisCostumer = server.finishedCostumers[i];
		allCostumersCalculatedArrivalTime += thisCostumer.calculatedRandomArrivalTime;
		allCostumersCalculatedServiceTime += thisCostumer.calculatedRandomServiceTime;
		allCostumersTimeSpentWaiting += thisCostumer.timeSpentWaiting;
		float arrival = thisCostumer.arrivalTime;
		float wait = thisCostumer.timeSpentWaiting - thisCostumer.calculatedRandomServiceTime;
		float randomArrival = thisCostumer.calculatedRandomArrivalTime;
		float randomService = thisCostumer.calculatedRandomServiceTime;
		myFile << "Costumer #"<< i <<" \t================================================================\n";
		myFile << "Arrived at "<< arrival <<", \tWaited for "<< wait <<" before being served.\n";
		myFile << "Randomized Arrival Time:"<< randomArrival <<" \tRandomized Servive Time: "<< randomService <<"\n\n";
		}

	float meanCalculatedArrival = allCostumersCalculatedArrivalTime/server.finishedCostumers.size();
	float meanCalculatedService = allCostumersCalculatedServiceTime/server.finishedCostumers.size();
	float meanTimeSpentWaiting = allCostumersTimeSpentWaiting/server.finishedCostumers.size();
	float meanQueueLengthsWithoutActualCostumer = server.summedQueueLengthsWithoutActualCostumer / totalTime;
	float meanQueueLengths = server.summedQueueLengths / totalTime;


	myFile << "\n\n";
	myFile << " =====================================================================\n";
	myFile << " ============================= M/M/1 QUEUE ===========================\n";
	myFile << " =====================================================================\n";
	myFile << " ===                                                               ===\n";
	myFile << " === Number of costumers: " <<server.finishedCostumers.size() <<"                          \t   ===\n";
	myFile << " === Arrival rate lamda: "<< lamda <<"                                \t   ===\n", lamda;
	myFile << " === Service times mu: 1/"<< mu <<"                                   \t   ===\n", mu;
	myFile << " === Total needed Time: "<< totalTime <<"                             \t   ===\n", totalTime;
	myFile << " === Average calculated arrival: "<< meanCalculatedArrival <<"                   \t   ===\n";
	myFile << " === Average calculated service: "<< meanCalculatedService <<"                   \t   ===\n";
	myFile << " === Average time spent waiting: "<< meanTimeSpentWaiting <<"                   \t   ===\n";
	myFile << " === Average queue length: "<< meanQueueLengths <<"                        \t   ===\n";
	myFile << " === Average queue length without the served one: "<< meanQueueLengthsWithoutActualCostumer <<"  \t   ===\n";
	myFile << " ===                                                               ===\n";
	myFile << " =====================================================================\n";
	myFile << " =====================================================================\n";

	myFile.close();
	}