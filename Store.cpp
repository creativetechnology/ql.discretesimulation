#pragma strict
#include "Store.h"

// CONSTRUCTORS ======================================
Store::Store(void)
	{
	}
Store::Store(int numberOfCostumers, float lamda, float mu)
	{
	Store::lamda = lamda;
	Store::mu = mu;
	costumersInStore.clear();
	for (int i=0; i<numberOfCostumers; i++)
		costumersInStore.push_back(createCostumer());
	
	Server *serverPtr = new Server();
	server = *serverPtr;
	QueueMath *qmPtr = new QueueMath();
	queueMath = *qmPtr;
	abstractTime = 0.0f;
	}


Store::~Store(void)
	{
	}

// METHODS ==========================================

void Store::runSimulation(void)
	{
	while(!costumersInStore.empty())
		{
		float timeSpanUntilNextArrival = costumersInStore.front().calculatedRandomArrivalTime;
		server.workFor(timeSpanUntilNextArrival);
		abstractTime += timeSpanUntilNextArrival;
		costumersInStore.front().arrivalTime = abstractTime;
		server.addToWaitQueue(costumersInStore.front());
		costumersInStore.erase(costumersInStore.begin());
		}
	abstractTime += server.finish();
	}

Costumer Store::createCostumer(void)
	{
	Costumer *returnCostumerPtr = new Costumer();
	Costumer returnCostumer = *returnCostumerPtr;
	delete returnCostumerPtr;
	returnCostumer.timeSpentWaiting = 0.0f;
	returnCostumer.calculatedRandomArrivalTime = queueMath.Exponential(lamda);
	returnCostumer.calculatedRandomServiceTime = queueMath.Exponential(mu);
	return returnCostumer;
	}
