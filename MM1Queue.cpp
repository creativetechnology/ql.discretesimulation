#pragma once
#include "MM1Queue.h"

MM1Queue::MM1Queue(void)
	{
	}

int main(int argc, char *argv[])
	{
	MM1Queue queue = *(new MM1Queue());
	queue.run();
	return 0;
	}

void	MM1Queue::run(void)
	{
	Store *storePtr = new Store(NUMBER_OF_COSTUMERS, LAMDA, MU);
	store = *storePtr;
	store.runSimulation();

	output = new Output();
	output->print(LAMDA, MU, store.abstractTime, store.server);
	output->outputToFile(LAMDA, MU, store.abstractTime, store.server);
	getchar();

	}
