#pragma strict
#include "Server.h"


Server::Server(void)
	{
	/**
	*
	*/
	timeUntilCurrentFinished = NULL;
	summedQueueLengths = 0.0f;
	summedQueueLengthsWithoutActualCostumer = 0.0f;
	}


Server::~Server(void)
	{
	}
/* std::list<Costumer> waitQueue; */
// METHODS ======================================
void Server::addToWaitQueue(Costumer costumer)
	{
	waitQueue.push_back(costumer);
	}

void Server::workFor(float timeSpan){
	for (float remainingTimeToWork=timeSpan; remainingTimeToWork>0.0f;){
		if ( waitQueue.empty() )
			remainingTimeToWork = 0.0f; // Nothing to do, just finish
		else
			{
			//	if we just finished serving a costumer, update timeUntilCurrentFinished from the next
			if (timeUntilCurrentFinished == NULL)
				timeUntilCurrentFinished = waitQueue.front().calculatedRandomServiceTime;
			// if (we have more than enough time to finish the current costumer)
			//		subtract (time to finish the current costumer) from (the time we have left)
			//		finish serving (current Costumer)
			if (remainingTimeToWork > timeUntilCurrentFinished){
				remainingTimeToWork -= timeUntilCurrentFinished; // update remainingTimeToWork
				for  (int i=0; i<waitQueue.size(); i++)
					waitQueue[i].timeSpentWaiting += timeUntilCurrentFinished;
				// MEANQUEUELENGTH CALC
				summedQueueLengths += waitQueue.size() * timeUntilCurrentFinished;
				summedQueueLengthsWithoutActualCostumer += (waitQueue.size()-1) * timeUntilCurrentFinished;
				// MEANQUEUELENGTH END
				finishCostumer();
				}
			// if (the time we have is just enough to finish the current costumer)
			//		set (the time we have left) to zero
			//		finish serving (current Costumer)
			else if (remainingTimeToWork == timeUntilCurrentFinished){
				remainingTimeToWork = 0.0f; // set remainingTimeToWork to none
				for  (int i=0; i<waitQueue.size(); i++)
					waitQueue[i].timeSpentWaiting += timeUntilCurrentFinished;
				// MEANQUEUELENGTH CALC
				summedQueueLengths += waitQueue.size() * timeUntilCurrentFinished;
				summedQueueLengthsWithoutActualCostumer += (waitQueue.size()-1) * timeUntilCurrentFinished;
				// MEANQUEUELENGTH END
				finishCostumer();
				}
			// if (the time we have is not enough to finish the current costumer)
			//		subtract (the time we have) from (the time to finish serving the current costumer)
			//		set (the time we have left) to zero
			else if (remainingTimeToWork < timeUntilCurrentFinished){
				timeUntilCurrentFinished -= remainingTimeToWork; // partly serve the current Costumer
				for  (int i=0; i<waitQueue.size(); i++)
					waitQueue[i].timeSpentWaiting += remainingTimeToWork;
				remainingTimeToWork = 0.0f; // set remainingTimeToWork to none
				// MEANQUEUELENGTH CALC
				summedQueueLengths += waitQueue.size() * remainingTimeToWork;
				summedQueueLengthsWithoutActualCostumer += (waitQueue.size()-1) * remainingTimeToWork;
				// MEANQUEUELENGTH END
				}
			}
		}
	}
float Server::finish(void)
	{
	float remainingTimeNeeded = 0.0f;
	for (int i=0; !waitQueue.empty(); i++)
		{
		if (timeUntilCurrentFinished == NULL)
			timeUntilCurrentFinished = waitQueue.front().calculatedRandomServiceTime;
		remainingTimeNeeded += timeUntilCurrentFinished;
		for  (int i=0; i<waitQueue.size(); i++)
					waitQueue[i].timeSpentWaiting += timeUntilCurrentFinished;
		finishCostumer();
		}
	return remainingTimeNeeded;
	}

void Server::finishCostumer(void)
	{
	finishedCostumers.push_back(waitQueue.front()); // add finished Costumer to finishedCostumers
	waitQueue.erase(waitQueue.begin());	// remove finished Costumer from waitQueue
	//waitQueue.begin()->timeSpentWaiting = 0.0f;
	timeUntilCurrentFinished = NULL; // indicates we just finished serving a costumer		
	}