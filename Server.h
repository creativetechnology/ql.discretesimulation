#pragma once
#include "Costumer.h"
#include <vector>


class Server
	{
	public:
		Server(void);
		~Server(void);

		void	addToWaitQueue(Costumer costumer);
		void	workFor(float timeSpan);
		float	finish(void);
		void	finishCostumer(void);

		std::vector<Costumer>	waitQueue;
		std::vector<Costumer>	finishedCostumers;
		float					timeUntilCurrentFinished;

		//One being served, 2 in line = QueueLength 3
		float					summedQueueLengthsWithoutActualCostumer;
		//One being served, 2 in line = QueueLength 2
		float					summedQueueLengths;
		
	};

