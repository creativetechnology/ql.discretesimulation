#pragma once
#include "Server.h"
#include <stdio.h>
#include <fstream>
#include <iostream>


class Output
	{
	public:
		Output(void);
		~Output(void);

		void print(float lamda, float mu, float totalTime, Server server);
		void outputToFile(float lamda, float mu, float totalTime, Server server);
	};

