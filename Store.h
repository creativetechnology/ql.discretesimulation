#pragma once
#include <vector>
#include "Server.h"
#include "Costumer.h"
#include "QueueMath.h"



class Store
	{
	public:
		Store(void);
		Store(int numberOfCostumers, float lamda, float mu);
		~Store(void);

		void			setupStore(int numberOfCostumers);
		void			runSimulation(void);
		Costumer		createCostumer(void);
		
		QueueMath				queueMath;
		Server					server;
		std::vector<Costumer>	costumersInStore;
		float					lamda;
		float					mu;
		float					abstractTime;

	};

